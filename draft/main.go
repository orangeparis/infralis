package draft

import (
	"io"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
)

var HeadersFilename = "../assets/headers"

type InfralisTable struct {
	Name   string
	Fields []string
}

type InfralisTables struct {
	Tables []InfralisTable
}

func (csv *InfralisTables) LoadFromCsv(data []byte, sep rune) (err error) {

	if sep != ';' {
		sep = ','
	}

	r := csv.NewReader(strings.NewReader(string(data)))
	r.Comma = sep
	r.Comment = '#'

	var headerFlag = false //   header status
	v.Headers = []string{}
	v.Lines = [][]string{}

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("%s\n", err.Error())
			return err
		}
		if headerFlag == false {
			// this is the first line
			headerFlag = true
			v.Headers = record
		} else {
			// add line
			v.Lines = append(v.Lines, record)
		}
	}
	return err
}

func (r *GitRepository) GetPlatform(name string) (data []byte, err error) {
	ptf := filepath.Join(r.Path, "ines-deployment", "vm", name)
	data, err = ioutil.ReadFile(ptf)
	if err != nil {
		log.Printf("cannot find csv file at %s\n", ptf)
		return data, err
	}
	return data, err
}

func main() {

	s := &InfralisTables{}

	data, err = ioutil.ReadFile()
	if err != nil {
		log.Printf("cannot find csv file at %s\n", ptf)
		return data, err
	}

	_ = s

}
