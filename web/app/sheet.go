package app

import (
	"html/template"
	"log"
	"net/http"

	repo "bitbucket.org/orangeparis/infralis/infralis"
	sheets "bitbucket.org/orangeparis/infralis/sheets"
)

/*
	handle generic sheet handler

	/sheets/<name.csv>


*/

// SheetHandler : handle  /sheets/<name>    eg /sheet/pdu-fa13.csv
func SheetHandler(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Path[len("/sheets/"):]
	data, err := repo.GetPlatform(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// select the handler
	//  eg pdu-fa13.csv --> pdu ,

	// force pdu as an example
	//sheet, err := sheets.NewPduSheet(name)

	// use generic sheet object
	sheet, err := sheets.NewSheet(name)
	sheet.LoadFromCsv(data, ',')
	// add select column
	sheet.AddLeftColumn("Select", "")

	headers := sheet.Headers
	sizes := sheet.SizeHeaders(10)

	tdata := struct {
		Platform string
		Headers  []string
		Sizes    []int
	}{
		Platform: name,
		Headers:  headers,
		Sizes:    sizes,
	}
	_ = tdata

	t, err := template.New("sheet.html").ParseFiles(TemplatePath("sheet"))
	if err != nil {
		log.Print("template compiling error: ", err)
	}
	err = t.Execute(w, tdata)
	if err != nil {
		log.Print("template executing error: ", err)
	}
	//fmt.Fprintf(w, "Hi from sheet handler, I love %s!", r.URL.Path[1:])
}
