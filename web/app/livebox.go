package app

import (
	"encoding/json"
	"html/template"
	"log"
	"net/http"

	"bitbucket.org/orangeparis/pickup"
	liveboxrepo "bitbucket.org/orangeparis/pickup/repository/livebox"
)

/*
	controlers for livebox

	/api/livebox/platforms
	/api//*
	/sheet/vm/*

*/

//
// livebox api
//
//  /api/liveboxplatforms
//  /api/livebox/<ptf>

// handle /apiplatforms return a json list of platforms
func apiPlatformsHandler(w http.ResponseWriter, r *http.Request) {

	platforms := liveboxrepo.Platforms()

	js, err := json.Marshal(platforms)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

}

// handle /api/livebox/<ptf> return a canonical csv ( comma separated)
func apiLiveboxCsvHandler(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Path[len("/api/livebox/"):]
	csv, err := liveboxrepo.GetPlatform(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// build livebox sheet from platform info
	sheet, err := pickup.NewLiveboxSheet2(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	err = sheet.LoadFromCsv(csv, ';')
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}
	// add Select column
	sheet.AddLeftColumn("Select", "")
	buffer, err := sheet.ToCsv(',')
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.Write([]byte(buffer))

}

// handle /api/platform/<name> return a json object
func apiPlatformHandler(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Path[len("/api/platform/"):]

	platform, err := liveboxrepo.GetPlatform(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	js, err := json.Marshal(platform)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)

}

//
// livebox sheet
//

// handle /sheet/livebox/<ptf>
func liveboxSheetHandler(w http.ResponseWriter, r *http.Request) {

	ptf := r.URL.Path[len("/sheet/livebox/"):]
	platform_data, err := liveboxrepo.GetPlatform(ptf)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	sheet, err := pickup.NewLiveboxSheet2(ptf)
	sheet.LoadFromCsv(platform_data, ';')
	// add select column
	sheet.AddLeftColumn("Select", "")

	headers := sheet.Headers
	sizes := sheet.SizeHeaders(10)

	data := struct {
		Platform string
		Headers  []string
		Sizes    []int
	}{
		Platform: ptf,
		Headers:  headers,
		Sizes:    sizes,
	}

	t, err := template.New("livebox.html").ParseFiles(TemplatePath("livebox"))
	if err != nil {
		log.Print("template compiling error: ", err)
	}
	err = t.Execute(w, data)
	if err != nil {
		log.Print("template executing error: ", err)
	}
	//fmt.Fprintf(w, "Hi from sheet handler, I love %s!", r.URL.Path[1:])
}
