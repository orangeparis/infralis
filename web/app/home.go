package app

import (
	"html/template"
	"log"
	"net/http"

	//liveboxrepo "bitbucket.org/orangeparis/pickup/repository/livebox"
	//vmrepo "bitbucket.org/orangeparis/pickup/repository/vm"
	repo "bitbucket.org/orangeparis/infralis/infralis"
)

func HomeHandler(w http.ResponseWriter, r *http.Request) {

	t, err := template.New("home.html").ParseFiles(TemplatePath("home"))
	if err != nil {
		log.Print("template compiling error: ", err)
	}
	data := struct {
		Repository string
		Platforms  []string
		//VmPlatforms      []string
	}{
		Repository: "ines",
		Platforms:  repo.Platforms(),
		//VmPlatforms:      vmrepo.Platforms(),
	}

	err = t.Execute(w, data)
	if err != nil {
		log.Print("template executing error: ", err)
	}

	//fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}
