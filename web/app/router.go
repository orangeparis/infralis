package app

import (
	"net/http"
)

func Routes() {

	//
	// static file server
	//
	// This works and strip "/static/" fragment from path
	fs := http.FileServer(http.Dir(StaticPath("")))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	//
	// home
	//
	http.HandleFunc("/", HomeHandler)

	// api csv
	http.HandleFunc("/api/csv/", ApiCsvHandler)
	// api update
	http.HandleFunc("/api/update/", ApiUpdateHandler)
	// api commit
	http.HandleFunc("/api/commit/", ApiCommitHandler)

	// sheets router
	http.HandleFunc("/sheets/", SheetHandler)

}
