package config

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

/*
	handle configuration for tables




*/

type Tables struct {
	All   []string
	Table map[string]*TableConfiguration
}

type TableConfiguration struct {
	Kind    string   // type of table : pdu,livebox,lan,nis
	Headers []string // headers
}

func NewTableConfiguration(kind string) (table *TableConfiguration, err error) {

	table = &TableConfiguration{Kind: kind}

	f1 := fmt.Sprintf("%s.fields", kind)
	o1 := viper.GetString(f1)
	table.Headers = NewCommaList(o1).List()

	return table, err
}

func NewTables() (tables *Tables, err error) {

	// get list of table kinds
	l := viper.GetString("headers.tables")
	all := NewCommaList(l).List()

	table := make(map[string]*TableConfiguration)
	tables = &Tables{
		All:   all,
		Table: table,
	}

	// fill the tables
	for _, kind := range tables.All {

		conf, err := NewTableConfiguration(kind)
		if err == nil {
			tables.Table[kind] = conf
		}
	}
	//tables.Table = table

	return tables, err

}

// WhatKind : guess the kind of table from a name  eg  pdu-fa13.csv -> pdu
func (t *Tables) WhatKind(name string) (kind string) {

	for _, kind := range t.All {

		// search string kind in name
		if strings.Contains(name, kind) {
			// found it
			return kind
		}
	}
	return ""
}

func (t *Tables) GetConfig(name string) (table *TableConfiguration) {
	kind := t.WhatKind(name)
	if kind != "" {
		table = t.Table[kind]
	}
	return table
}
