package config

/*

	handle comma separated list


	usage:
			l := NewCommaList("hello,there").List()
			// l == ["hello","there"]

*/

import "strings"

type StringLister interface {
	List() []string
}

// StringList : a list of string elements separated with a sep ( , ; )
type StringList struct {
	Text string
	Sep  rune
}

// NewCommaList : create a comma separated list
func NewCommaList(text string) *StringList {
	return &StringList{Text: text, Sep: ','}
}

// List : get a []string representation
func (s *StringList) List() []string {
	return strings.Split(s.Text, string(s.Sep))
}

// String : get a string representation
func (s *StringList) String() string {
	return s.Text
}
