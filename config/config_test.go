package config_test

import (
	"testing"

	"bitbucket.org/orangeparis/infralis/config"
	"github.com/spf13/viper"
)

func TestConfig(t *testing.T) {

	config.Load()
	data := viper.AllSettings()
	_ = data
	repo := viper.GetString("repository.url")
	println(repo)
	// if repo != "/tmp/ines" {
	// 	t.Errorf("repo was incorrect, got: %s, want: %s", repo, "/tmp/ines")
	// }

	tmp := viper.GetString("vars.tmp")
	println(tmp)
	if tmp != "/tmp/ines" {
		t.Errorf("tmp was incorrect, got: %s, want: %s", tmp, "/tmp/ines")
	}

	// setting vars.tmp
	viper.Set("vars.tmp", "/tmp/ines_modified")

	tmp = viper.GetString("vars.tmp")
	println(tmp)
	if tmp != "/tmp/ines_modified" {
		t.Errorf("repo was incorrect, got: %s, want: %s", tmp, "/tmp/ines_modified")
	}

	println(viper.GetString("appName"))

}

func TestHeaders(t *testing.T) {

	config.Load()
	data := viper.AllSettings()
	_ = data

	t1 := viper.GetString("pdu.fields")
	if t1[:8] != "Nom PDU," {
		t.Fail()
		return
	}

	tables := viper.GetString("headers.tables")
	if tables[:4] != "pdu," {
		t.Fail()
		return
	}

}

func TestTableConfiguration(t *testing.T) {

	config.Load()
	//data := viper.AllSettings()

	tables, _ := config.NewTables()

	if tables.Table["pdu"].Kind != "pdu" {
		t.Fail()
		return
	}

	//println(tables)

	k1 := tables.WhatKind("livebox-fa13.csv")
	if k1 != "livebox" {
		t.Fail()
		return
	}

	pduCnf := tables.Table[k1]
	if pduCnf.Kind != "livebox" {
		t.Fail()
		return
	}

	//println(pduCnf.Kind)

}
