package tools

import (
	"encoding/csv"
	"encoding/json"
	"io"
	"log"
	"strings"
)

// CsvTable a csv file
type CsvTable struct {
	Headers []string
	Lines   [][]string
}

// NewCsvTable : create a new csv table
func NewCsvTable(headers []string, lines [][]string) (table *CsvTable) {
	table = &CsvTable{
		Headers: headers,
		Lines:   lines,
	}
	return table
}

// LoadFromCsv load from csv content ( first line are headers)
func (v *CsvTable) LoadFromCsv(data []byte, sep rune) (err error) {

	if sep != ';' {
		sep = ','
	}

	r := csv.NewReader(strings.NewReader(string(data)))
	r.Comma = sep
	r.Comment = '#'

	var headerFlag = false //   header status
	v.Headers = []string{}
	v.Lines = [][]string{}

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Printf("%s\n", err.Error())
			return err
		}
		if headerFlag == false {
			// this is the first line
			headerFlag = true
			v.Headers = record
		} else {
			// add line
			v.Lines = append(v.Lines, record)
		}
	}
	return err
}

// ToCsv : return Csv representation
func (v CsvTable) ToCsv(sep rune) (data string, err error) {

	if sep != ';' {
		sep = ','
	}
	in := v.Lines
	out := &strings.Builder{}
	w := csv.NewWriter(out)
	//w.Comma = ','
	w.Comma = sep

	w.WriteAll(in) // calls Flush internally
	if err := w.Error(); err != nil {
		log.Fatalln("error writing csv:", err)
	}
	//fmt.Println(out.String())
	data = out.String()

	return data, err
}

// ToJson : return json
func (v *CsvTable) ToJson() (data []byte, err error) {
	return json.Marshal(v.Lines)
}

// SizeHeaders : compute headers size
func (v *CsvTable) SizeHeaders(coef int) (headerSizes []int) {

	// coef for ratio text lenght -> pixel
	if coef == 0 {
		coef = 10
	}
	for i, header := range v.Headers {
		// minimum size of header is len of the label
		min := len(header)
		headerSizes = append(headerSizes, min)
		for _, line := range v.Lines {
			// if len element > min : adjust it
			n := len(line[i])
			if n > min {
				headerSizes[i] = n
			}
		}
		// apply coef to text length to get lenght in pixels
		headerSizes[i] = headerSizes[i] * coef
	}
	return headerSizes
}

// AddLeftColumn :  add a column to the left
func (v *CsvTable) AddLeftColumn(label string, content string) {
	h, c := AddLeftColumn(v.Headers, v.Lines, label, content)
	v.Headers = h
	v.Lines = c
}

// DeleteLeftColumn  : remove the left column
func (v *CsvTable) DeleteLeftColumn() {
	h, c := DeleteLeftColumn(v.Headers, v.Lines)
	v.Headers = h
	v.Lines = c
}
