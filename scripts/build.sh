
rm -rf ./infralis
# install go dependencies
go get gopkg.in/src-d/go-git.v4
go get gopkg.in/src-d/go-git.v4/plumbing/object
go get gopkg.in/src-d/go-git.v4/plumbing/transport/http
go get github.com/spf13/viper
go get bitbucket.org/orangeparis/infralis/config
go get bitbucket.org/orangeparis/infralis/infralis
go get bitbucket.org/orangeparis/infralis/web/app

# compile 
export CGO_ENABLED=0
export GOARCH=amd64
# linux
export GOOS=linux
go build -v -o ./infralis/infralis-$GOOS-$GOARCH ../cmd/infralis/main.go
# windows
export GOOS=windows
go build -v -o ./infralis/infralis-$GOOS-$GOARCH.exe ../cmd/infralis/main.go
# add elements
cp ./config.toml ./infralis
cp -R ../web/static ./infralis
cp -R ../web/templates ./infralis
# build the zip
cd infralis; zip -r infralis.zip *
# compile to osx
export GOOS=darwin
cd .. ;go build -v -o ./infralis/infralis-$GOOS-$GOARCH ../cmd/infralis/main.go

# for alpine
#CC=$(which musl-gcc) go build --ldflags '-w -linkmode external -extldflags "-static"' server.go