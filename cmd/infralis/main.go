package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"bitbucket.org/orangeparis/infralis/config"
	repo "bitbucket.org/orangeparis/infralis/infralis"
	"bitbucket.org/orangeparis/infralis/web/app"
)

var templates *template.Template

func main() {

	// get config
	config.Load()

	// set the infralis repository
	r := repo.NewGitRepositoryFromConfig()
	path, err := r.Clone()
	if err != nil {
		log.Printf("Error setting infralis repository: %s\n", err.Error())
		log.Fatal(err)
	}
	// update the temporary path in config
	config.Set("vars.tmp", path)
	repo.SetRepository(r)
	defer repo.Close()

	platforms := repo.Platforms()
	_ = platforms

	// declare web applcation routes
	app.Routes()

	// starts the web server
	port := config.GetString("web.port")
	addr := fmt.Sprintf(":%s", port)

	log.Printf("start server at %s", addr)
	log.Fatal(http.ListenAndServe(addr, nil))
}
