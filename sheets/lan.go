package sheets

import (
	"encoding/json"

	tools "bitbucket.org/orangeparis/infralis/csvtable"
)

/*

	handle lan info

		Table,Position,Brassage Lan,Brassage EX,IP Admin,Livebox ref


*/

// type LanSheet struct {
// 	*tools.CsvTable

// 	Name string // Name of the collection
// 	Key  string
// }

// // NewLanSheet : create a new PDU sheet
// func NewLanSheet(name string) (pdu *PduSheet, err error) {

// 	table := &tools.CsvTable{}
// 	pdu = &PduSheet{CsvTable: table, Name: name, Key: "Nom PDU"}
// 	return pdu, err
// }

type LanSheet struct {
	*Sheet
}

// NewLanSheet : create a new Lan sheet
func NewLanSheet(name string, key string) (lan *LanSheet, err error) {

	table := &tools.CsvTable{}
	sheet := &Sheet{CsvTable: table, Name: name, Key: key}
	lan = &LanSheet{sheet}
	return lan, err
}

// ToJson : intercept CsvTable.ToJson()
func (v *LanSheet) ToJson() (data []byte, err error) {
	//log.Printf("pdu Interception\n")
	return json.Marshal(v.Lines)
}

// // GetName : return name
// func (v *LanSheet) GetName() string {
// 	return v.Name
// }
