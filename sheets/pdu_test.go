package sheets_test

import (
	"fmt"
	"io/ioutil"
	"log"
	"testing"

	pickup "bitbucket.org/orangeparis/infralis/sheets"
)

var pdufilename = "../mock/samplepdu.csv"

func TestLoadPduSheet(t *testing.T) {

	data, err := ioutil.ReadFile(pdufilename)
	if err != nil {
		log.Fatal(err)
	}

	sheet, _ := pickup.NewPduSheet("samplepdu")
	name := sheet.GetName()
	_ = name
	err = sheet.LoadFromCsv(data, ',')
	if err != nil {
		log.Fatal(err)
	}

	data, err = sheet.ToJson()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(data))

	buffer, err := sheet.ToCsv(';')
	if err != nil {
		log.Fatal(err)
	}
	_ = buffer
	fmt.Println(buffer)

	// compute headers size
	sizes := sheet.SizeHeaders(10)
	_ = sizes
	//fmt.Println(sizes)

	sheet.AddLeftColumn("Select", "")
	buffer, err = sheet.ToCsv(';')
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(buffer)

	if len(sheet.Headers) != 8 {
		t.Fail()
		return
	}

	sheet.DeleteLeftColumn()
	if len(sheet.Headers) != 7 {
		t.Fail()
		return
	}

}
