package sheets

/*

	handle a pdu sheet


		Nom PDU,Socket,Table,Position,IP Admin,Adresse Mac,Livebox ref


*/

import (
	"encoding/json"

	tools "bitbucket.org/orangeparis/infralis/csvtable"
)

type PduSheet struct {
	*tools.CsvTable

	Name string // Name of the collection
	Key  string
}

// NewPduSheet : create a new PDU sheet
func NewPduSheet(name string) (pdu *PduSheet, err error) {

	table := &tools.CsvTable{}
	pdu = &PduSheet{CsvTable: table, Name: name, Key: "Nom PDU"}
	return pdu, err
}

// ToJson : intercept CsvTable.ToJson()
func (v *PduSheet) ToJson() (data []byte, err error) {
	//log.Printf("pdu Interception\n")
	return json.Marshal(v.Lines)
}

// GetName : return name
func (v *PduSheet) GetName() string {
	return v.Name
}
