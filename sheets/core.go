package sheets

/*

	handle a sheet


		header1,header2


*/

import (
	tools "bitbucket.org/orangeparis/infralis/csvtable"
)

// Sheet an extension of csvTable
type Sheet struct {
	*tools.CsvTable

	Name string // Name of the collection
	Key  string
}

// NewSheet : create a new PDU sheet
func NewSheet(name string) (sheet *Sheet, err error) {

	table := &tools.CsvTable{}
	sheet = &Sheet{CsvTable: table, Name: name, Key: "header1"}
	return sheet, err
}

// GetName : return name
func (s *Sheet) GetName() string {
	return s.Name
}
