# Infralis #


Liste des infras ines


visualisation/edition des tables de configuration des plateformes


recupere les données des table depuis un repository git de table csv


# tables

* LIVEBOX-FA13
* E2E-NIS-FA13
* PDU-FA13
* LAN-FA13



# dependencies

    go get github.com/spf13/viper

    go get gopkg.in/src-d/go-git.v4
    go get gopkg.in/src-d/go-git.v4/plumbing
    go get gopkg.in/src-d/go-git.v4/plumbing/transport/http



# build from source

    git clone https://cocoon_bitbucket@bitbucket.org/orangeparis/infralis.git infralis
    cd infralis
    ./build.sh
 
 then  unzip the infralis.zip in a new directory 
 and launch the executable for your platform eg ./infralis-linux-...
 