package repository_test

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"testing"

	"bitbucket.org/orangeparis/infralis/config"
	"bitbucket.org/orangeparis/infralis/repository"
)

var sampleGit = "./samplegit"

func TestGitRepository(t *testing.T) {

	config.Set("repository.url", sampleGit)
	config.Load()

	r := repository.NewBaseGitRepositoryFromConfig()
	repository.SetRepository(r)
	defer repository.Close()

	// clone repository and get path of temporary dict
	path, err := repository.Clone()
	if err != nil {
		log.Printf("error: %s\n", err.Error())
		t.Fail()
		return
	}
	// update the temporary path in config
	config.Set("vars.tmp", path)

	// recreate repository object with the new path
	r = repository.NewBaseGitRepositoryFromConfig()
	repository.SetRepository(r)

	// check README.md file in cloned repository
	fpath := filepath.Join(r.Path, "README.md")
	if _, err := os.Stat(fpath); !os.IsNotExist(err) {
		// ok
		log.Printf("README file present\n")
	} else {
		log.Printf("README file not found: %s\n", err.Error())
		t.Fail()
		return
	}

	err = repository.Pull()
	if err != nil {
		log.Printf("%s\n", err.Error())
	}

	root := repository.Root()
	log.Printf("root path is %s\n", root)

}

func TestGitRepositoryCommit(t *testing.T) {

	config.Set("repository.url", sampleGit)
	config.Load()

	r := repository.NewBaseGitRepositoryFromConfig()
	// clone repository and get path of temporary dict
	path, err := r.Clone()
	if err != nil {
		log.Printf("error: %s\n", err.Error())
		t.Fail()
		return
	}
	// update the temporary path in config
	config.Set("vars.tmp", path)

	// recreate repository object with the new path
	//r = repository.NewBaseGitRepositoryFromConfig()
	repository.SetRepository(r)
	defer repository.Close()

	// check README.md file in cloned repository
	fpath := filepath.Join(r.Path, "README.md")
	if _, err := os.Stat(fpath); !os.IsNotExist(err) {
		// ok
		log.Printf("README file present\n")
	} else {
		log.Printf("README file not found: %s\n", err.Error())
		t.Fail()
		return
	}

	// modify README file
	data, err := ioutil.ReadFile(fpath)
	if err != nil {
		log.Printf("cannot README file at: %s\n", err.Error())
		t.Fail()
		return
	}
	line := []byte("\n this is a change\n")
	//slice := append([]byte(data), "...)
	data = append(data, line...)
	err = ioutil.WriteFile(fpath, data, 0644)
	if err != nil {
		log.Printf("cannot README file at: %s\n", err.Error())
		t.Fail()
		return
	}

	// commit change
	hash, err := r.Commit("my change")
	if err != nil {
		log.Printf("cannot commit : %s\n", err.Error())
		t.Fail()
		return
	}
	_ = hash

}
