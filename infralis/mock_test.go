package infralis_test

import (
	"fmt"
	"testing"

	"bitbucket.org/orangeparis/infralis/infralis"
)

func TestMockRepository(*testing.T) {

	r := infralis.NewMockRepository("")
	infralis.SetRepository(r)

	pls := infralis.Platforms()
	fmt.Println(pls)

	data, _ := infralis.GetPlatform("pdu-fa13.csv")
	fmt.Println(string(data))

}
