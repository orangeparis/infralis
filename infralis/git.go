package infralis

import (
	"io/ioutil"
	"log"
	"path/filepath"

	"bitbucket.org/orangeparis/infralis/config"
	"bitbucket.org/orangeparis/pickup/repository"
)

/*
	implements an infralis git repository
*/

// GitRepository : an infralis git repository
type GitRepository struct {
	repository.BaseGitRepository // a basic git implementation ( Close/Clone ...)
}

// NewGitRepository : create a git repository handler
func NewGitRepository(url string, path string, user string, password string) (git *GitRepository) {
	base := repository.BaseGitRepository{Url: url, Path: path, User: user, Password: password}
	git = &GitRepository{base}
	return git
}

// NewGitRepositoryFromConfig : create a git repository handler from config
func NewGitRepositoryFromConfig() (repository *GitRepository) {
	repository = NewGitRepository(
		config.GetString("repository.url"),
		config.GetString("vars.tmp"),
		config.GetString("repository.user"),
		config.GetString("repository.password"))
	repository.AuthorName = config.GetString("repository.author")
	repository.AuthorEmail = config.GetString("repository.email")
	return repository

}

// Platforms : return a list of platform directories
func (r *GitRepository) Platforms() (files []string) {

	//base := filepath.Join(r.Path, "infralis")
	base := r.Path
	files, err := ListCsfFiles(base)
	if err != nil {
		log.Printf("cannot list platforms: %s", err.Error())
	}
	return files
}

// GetPlatform : return the platform objet with this name
func (r *GitRepository) GetPlatform(name string) (data []byte, err error) {

	//  compute csv filename <root>/<name>.csv
	pfile := filepath.Join(r.Path, name)
	data, err = ioutil.ReadFile(pfile)
	if err != nil {
		log.Printf("cannot find livebox csv file at %s\n", pfile)
		return data, err
	}

	return data, err
}

func (r *GitRepository) Root() string {
	return r.Path
}
