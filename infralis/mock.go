package infralis

import (
	"bitbucket.org/orangeparis/pickup/repository"
)

/*
	implements a mock repository for infralis

*/

var platforms = []string{"pdu-fa13.csv", "e2e-nis-fa13.csv", "livebox-fa13.csv", "lan-fa13.csv"}

var pdufa13Csv = `\
Nom PDU,Socket,Table,Position,IP Admin,Adresse Mac,Livebox ref
PDU-M1-P6,Soket 4,M1,6,192.168.100.52,88:B6:25:01:66:41,"livebox-5d40 (livebox 3, 88:B6:25:01:66:41)"
PDU-M1-P7,Soket 4,M1,6,192.168.100.53,88:B6:25:01:66:42,"livebox-5d40 (livebox 3, 88:B6:25:01:66:42)"`

// MockRepository : a mock of a git repository
type MockRepository struct {
	repository.MockRepository // a basic git implementation ( Close/Clone ...)
	//Base                      string // the Base directory , each subdiretory is a platform
}

// NewMockRepository : create a mock of a git repository
func NewMockRepository(base string) (repo *MockRepository) {
	model := repository.MockRepository{Path: base}
	repo = &MockRepository{model}
	return repo
}

//
// implements Repository interface
//

// Platforms : return a list of platform name
func (r *MockRepository) Platforms() (list []string) {
	return platforms
}

// GetPlatform : return the platform objet with this name
func (r *MockRepository) GetPlatform(name string) (data []byte, err error) {

	// return always pdufa13 content
	return []byte(pdufa13Csv), err

}

func (r *MockRepository) Root() string {
	return r.Path
}
