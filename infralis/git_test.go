package infralis_test

import (
	"fmt"
	"log"
	"testing"

	"bitbucket.org/orangeparis/infralis/config"
	infra "bitbucket.org/orangeparis/infralis/infralis"
)

var fixtureRepository = "https://bitbucket.org/orangeparis/infralis-fixture"

func TestGitRepository(t *testing.T) {

	config.Set("repository.url", fixtureRepository)
	config.Load()

	r := infra.NewGitRepositoryFromConfig()
	infra.SetRepository(r)
	defer infra.Close()

	path, err := infra.Clone()
	if err != nil {
		log.Printf("Error: %s\n", err.Error())
		t.Fail()
		return
	}
	// update the temporary path in config
	config.Set("vars.tmp", path)

	// recreate repository object
	r = infra.NewGitRepositoryFromConfig()
	infra.SetRepository(r)

	pls := infra.Platforms()
	fmt.Println(pls)

	if len(pls) <= 0 {
		log.Printf("cannot find platforms")
		t.Fail()
		return
	}
	demo := pls[0]

	data, _ := infra.GetPlatform(demo)
	fmt.Println(string(data))

}
