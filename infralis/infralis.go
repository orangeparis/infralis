package infralis

import (
	"io/ioutil"
	"strings"

	"bitbucket.org/orangeparis/pickup/repository"
)

/*

	an infralis git repository
	implements
		BaseRepository
		InfralisRepository


*/

// InfralisRepository : a git like repository of platform configurations
type InfralisRepository interface {
	repository.Repository               // Close(),Clone(), ....
	Platforms() []string                // List platforms
	GetPlatform(string) ([]byte, error) // get a Platform

}

// local variable for injection dependency
var impl InfralisRepository

// SetRepository : setting the repository ( dependecy injection )
func SetRepository(repository InfralisRepository) {
	impl = repository
}

// Close  clear temporary directory
func Close() {
	impl.Close()
}

func Clone() (path string, err error) {
	return impl.Clone()
}

// Root : return the path where to to find the cloned files
func Root() (path string) {
	return impl.Root()
}

func Pull() (err error) {
	return impl.Pull()
}

// Commit : commit changes to repository
func Commit(msg string) (hash string, err error) {
	return impl.Commit(msg)
}

// Platforms : return a list of platform name
func Platforms() (list []string) {
	return impl.Platforms()
}

//GetPlatform : return the platform objet with this name
func GetPlatform(name string) ([]byte, error) {
	return impl.GetPlatform(name)
}

// helpers

// ListCsfFiles : return list of csv files
func ListCsfFiles(root string) ([]string, error) {
	var files []string
	fileInfo, err := ioutil.ReadDir(root)
	if err != nil {
		return files, err
	}
	for _, file := range fileInfo {
		name := file.Name()
		if name[0] == '.' {
			// skip .*
			continue
		}
		if strings.HasSuffix(name, ".csv") {
			// keep only .csv extension
			files = append(files, name)
		}
	}
	return files, nil
}
